import { createAction, props } from '@ngrx/store'
import { UserModel } from '../../models/index'

export const createUser = createAction('create user',
    props<{ user: UserModel }>()
)
export const editUser = createAction('edit user',
    props<{ user: UserModel }>()
)
export const deleteUser = createAction('delete user',
    props<{ user: UserModel }>()
)