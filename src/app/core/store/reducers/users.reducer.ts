import { Action, createReducer, on } from '@ngrx/store'
import * as actions from '../actions/users.actions'
import { UserModel } from '../../models/index'

export interface State {
    users: UserModel[],
}

export const initialState: State = {
    users: [],
};

const usersReducer = createReducer(
    initialState,
    on(actions.createUser, (state, { user }) => ({
        ...state,
        users: state.users.push(user)
        // users: [   
        //     {
        //         id: '1',
        //         firstName: 'Sebastian',
        //     },
        //     {
        //         id: '2',
        //         firstName: 'George',
        //     }
        // ]
    })),
);

export function UserReducer(state: State | undefined, action: Action) {
    return usersReducer(state, action);
}

export const getUsers = (state: State) => state.users;

