import {
    createSelector,
    createFeatureSelector,
    Action,
    ActionReducerMap,
} from '@ngrx/store';
import { InjectionToken } from '@angular/core';

import * as fromUser from './users.reducer';

import { UserModel } from '../../models/index';

export interface State {
    user: fromUser.State,
}
export const ROOT_REDUCERS = new InjectionToken<ActionReducerMap<State, Action>>
    ('Root reducers token', {
        factory: () => ({
            user: fromUser.UserReducer,
        }),
    });

export const getUserState = createFeatureSelector<State, fromUser.State>(
    'user'
);

export const getUsers = createSelector(
    getUserState,
    fromUser.getUsers
);
