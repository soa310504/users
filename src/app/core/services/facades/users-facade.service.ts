import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromAppRoot from '../../store/reducers/index';
import { UserModel } from '../../models/index';
import * as action from '../../store/actions/users.actions'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdsFacadeService {

  constructor(
    private store: Store<fromAppRoot.State>,
  ) { }

  createUser(user: UserModel) {
    this.store.dispatch(action.createUser({ user }))
  }

  getUsers(): Observable<UserModel[]> {
    return this.store.select(Store.)
  }
}
